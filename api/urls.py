from django.contrib import admin
from django.urls import path
from api import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('register/', views.register_user, name='register_user'),
    path('list_user', views.List_user.as_view()),
    path('register', views.Register.as_view()),
    path('login', views.Login.as_view()),
    path('devices', views.GetDevices.as_view()),
    path('phoneupdate', views.PhoneUpdate.as_view()),
    path('activate', views.Activate.as_view()),
    path('audio', views.Audio_stor.as_view()),
    path('images', views.Image_stor.as_view()),
    path('uploaddashboardImage', views.UploadDashboardImage.as_view()),
    path('getdashboardImage', views.GetDashboardImage.as_view()),
    path('deletedashboardImage', views.DeleteDashboardImage.as_view()),
    path('updatedashboardImage', views.UpdateDashboardImage.as_view()),
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
