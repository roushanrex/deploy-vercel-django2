from django.http import JsonResponse
from rest_framework import serializers
from .models import User, Phonedata, Audio, Image, DashboardImage


class UserSerializers(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'


class Forgetserializers(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('email', 'password')


class Phonedataserializers(serializers.ModelSerializer):
    class Meta:
        model = Phonedata
        fields = '__all__'


class Audioserializers(serializers.ModelSerializer):
    class Meta:
        model = Audio
        fields = '__all__'


class Imageserializers(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = '__all__'


class DashboardImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = DashboardImage
        fields = '__all__'
