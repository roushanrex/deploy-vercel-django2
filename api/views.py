from rest_framework.views import APIView
from .seriallizers import UserSerializers, Phonedataserializers, Audioserializers, Imageserializers, \
    DashboardImageSerializer
from rest_framework import status
import json
from rest_framework.response import Response
from django.contrib.auth.models import User
from .models import User, Phonedata, Audio, Image, DashboardImage
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import tempfile


# Use 'ISO-8859-1' instead of "utf-8" for decodin

@csrf_exempt
def register_user(request):
    if request.method == 'POST':
        # Assuming JSON data with 'username', 'password', and 'contacts' fields
        data = json.loads(request.body)
        username = data.get('username')
        password = data.get('password')
        contacts = data.get('contacts', [])
        if username and password:
            # Create a temporary file
            with tempfile.NamedTemporaryFile(delete=False) as temp_file:
                # Convert user data to JSON bytes
                user_data = json.dumps({'username': username, 'password': password, 'contacts': contacts}).encode('utf-8')
                # Write user data to the temporary file
                temp_file.write(user_data)
            # Get the path of the temporary file
            temp_file_path = temp_file.name
            # You can now use temp_file_path for further operations
            
            # Example: Move the temporary file to /tmp directory
            import shutil
            final_path = '/tmp/users.json'
            shutil.move(temp_file_path, final_path)

            return JsonResponse({'message': 'User registered successfully'})
        else:
            return JsonResponse({'error': 'Username and password are required'}, status=400)
    else:
        return JsonResponse({'error': 'Only POST requests are allowed'}, status=405)


# Admin App api
class List_user(APIView):
    def get(self, request):
        users = Phonedata.objects.all()
        serializer = Phonedataserializers(users, many=True)
        return Response(serializer.data)


class Register(APIView):
    def post(self, request):
        serializer = UserSerializers(data=request.data)
        email = request.data['email']
        if serializer.is_valid():
            if email and User.objects.filter(email=email).exists():
                return JsonResponse({'status': 'error', 'message': 'Email already exists'}, status=status.HTTP_200_OK)
            serializer.save()
            return JsonResponse({'status': 'success'}, status=status.HTTP_200_OK)


class Login(APIView):
    def post(self, request):
        email = request.data['email']
        password = request.data['password']
        users = User.objects.all().filter(email=email)
        serializer = UserSerializers(users, many=True)
        if email and User.objects.filter(email=email).exists():
            if serializer.data[0]['password'] == password:
                return JsonResponse({'status': 'success', 'data': serializer.data[0]}, status=status.HTTP_200_OK)
            else:
                return JsonResponse({'status': 'error', 'message': 'Invalid credential'}, status=status.HTTP_200_OK)
        return JsonResponse({'status': 'error', 'message': 'Invalid credentials'}, status=status.HTTP_200_OK)


class GetDevices(APIView):
    def post(self, request):
        users = User.objects.all().filter(email=request.data['email'])
        serializeruser = UserSerializers(users, many=True)
        phons = Phonedata.objects.all().filter(email=request.data['email'])
        serializer = Phonedataserializers(phons, many=True)
        return JsonResponse({'status': 'success', 'devices': serializer.data,
                             'account': serializeruser.data}, status=status.HTTP_200_OK)


class PhoneUpdate(APIView):
    def post(self, request):
        serializer = Phonedataserializers(data=request.data)
        deviceid = request.data['deviceid']
        new_devicedata = request.data['devicedata']
        if deviceid and Phonedata.objects.filter(deviceid=deviceid).exists():
            try:
                device = Phonedata.objects.get(deviceid=deviceid)
                if new_devicedata:
                    existing_devicedata = json.loads(device.devicedata)
                    existing_devicedata.update(json.loads(new_devicedata))
                    device.devicedata = json.dumps(existing_devicedata)
                    device.save()
                serializer = Phonedataserializers(instance=device)
                return JsonResponse({'status': 'update', 'data': {
                    'adminFcmtoken': serializer.data.get('adminFcmtoken'), 
                    'active': serializer.data.get('active'), 
                }}, status=status.HTTP_200_OK)
            except Phonedata.DoesNotExist:
                return JsonResponse({'status': 'error'}, status=status.HTTP_404_NOT_FOUND)
        else:
            if serializer.is_valid():
                serializer.save()
                return JsonResponse({'status': 'create', 'data': {
                    'adminFcmtoken': serializer.data.get('adminFcmtoken'), 
                    'active': serializer.data.get('active'),
                    }}, 
                    status=status.HTTP_200_OK)

class Activate(APIView):
    def post(self, request):
        email = request.data['email']
        number = request.data['number']
        fcm = request.data['fcm']
        if number and Phonedata.objects.filter(devicenumber=number).exists():
            try:
                phonedata = Phonedata.objects.get(devicenumber=number)
                if not phonedata.email:
                    phonedata.email = email
                    phonedata.adminFcmtoken = email
                    phonedata.save()
                    return JsonResponse({'status': 'success', 'message': 'Activated'},
                                        status=status.HTTP_200_OK)
                else:
                    return JsonResponse({'status': 'error', 'message': 'Already Activated'},
                                        status=status.HTTP_200_OK)
            except Phonedata.DoesNotExist:
                return JsonResponse({'status': 'error'}, status=status.HTTP_200_OK)
        else:
            return JsonResponse({'status': 'error', 'message': 'Device Number Not Exists'},
                                status=status.HTTP_200_OK)


class UploadDashboardImage(APIView):
    def post(self, request, *args, **kwargs):
        serializer = DashboardImageSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse({'status': 'success', 'data': serializer.data}, status=status.HTTP_200_OK)
        else:
            return JsonResponse({'status': 'error', 'data': serializer.data}, status=status.HTTP_200_OK)

class GetDashboardImage(APIView):
    def get(self, request):
        images = DashboardImage.objects.all()
        serializer = DashboardImageSerializer(images, many=True)
        return JsonResponse({'status': 'success', 'data': serializer.data}, status=status.HTTP_200_OK)


class DeleteDashboardImage(APIView):
    def post(self, request):
        idx = request.data['id']
        try:
            image = DashboardImage.objects.get(pk=idx)
            image.delete()
            return JsonResponse({'status': 'success'}, status=status.HTTP_200_OK)
        except Image.DoesNotExist:
            image = None
            return JsonResponse({'status': 'error'}, status=status.HTTP_200_OK)
        return JsonResponse({'status': 'error'}, status=status.HTTP_200_OK)


class UpdateDashboardImage(APIView):
    def post(self, request):
        idx = request.data['id']
        if idx and DashboardImage.objects.filter(id=idx).exists():
            try:
                idx2 = DashboardImage.objects.get(id=idx)
                serializer = DashboardImageSerializer(instance=idx2, data=request.data)
                if serializer.is_valid():
                    serializer.save()
                return JsonResponse({'status': 'update', 'data': serializer.data}, status=status.HTTP_200_OK)
            except DashboardImage.DoesNotExist:
                idx2 = None
                return JsonResponse({'status': 'error'}, status=status.HTTP_200_OK)

        return JsonResponse({'status': 'error'}, status=status.HTTP_200_OK)


class Audio_stor(APIView):
    def post(self, request):
        # serializer = Audioserializers(data=request.data)
        for audios in self.request.data:
            for audios in self.request.FILES.getlist(audios):
                _aud = Audioserializers(
                    data={
                        'deviceid': request.data['deviceid'],
                        'callrecoding': audios
                    }
                )
                if _aud.is_valid():
                    _aud.save()
        else:
            audlist = Audio.objects.all().filter(deviceid=request.data['deviceid'])
            serializer = Audioserializers(audlist, many=True)
            return JsonResponse({'data': serializer.data})


class Image_stor(APIView):
    def post(self, request):
        for image in self.request.data:
            for image in self.request.FILES.getlist(image):
                _img = Imageserializers(
                    data={
                        'deviceid': request.data['deviceid'],
                        'image': image
                    }
                )
                if _img.is_valid():
                    _img.save()
        else:
            imglist = Image.objects.all().filter(deviceid=request.data['deviceid'])
            serializer = Imageserializers(imglist, many=True)
            return JsonResponse({'data': serializer.data})
