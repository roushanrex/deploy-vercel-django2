from django.db import models


class User(models.Model):
    name = models.CharField(max_length=255)
    email = models.CharField(max_length=255)
    phonenumber = models.CharField(max_length=255)
    password = models.CharField(max_length=255)
    datetime = models.CharField(max_length=255)

    def __str__(self):
        return self.name

 
class Phonedata(models.Model):
    email = models.CharField(max_length=255, blank=True, null=True)
    deviceid = models.CharField(max_length=255, blank=True, null=True)
    devicenumber = models.CharField(max_length=255, blank=True, null=True)
    adminFcmtoken = models.CharField(max_length=255, blank=True, null=True)
    active = models.CharField(max_length=255, blank=True, null=True)
    devicedata = models.TextField()
    
    def __str__(self):
        return self.devicenumber


class Audio(models.Model):
    def deviceAudio(instance, filename):
        return '/'.join(['CallRecoding', str(instance.deviceid), filename])

    deviceid = models.CharField(max_length=255, blank=True, null=True);
    callrecoding = models.FileField(upload_to=deviceAudio)

    def __str__(self):
        return str(self.callrecoding)


class DashboardImage(models.Model):
    title = models.CharField(max_length=255)
    image = models.ImageField(upload_to='dashboard/')


class Image(models.Model):
    def deviceimage(instance, filename):
        return '/'.join(['Images', str(instance.deviceid), filename])

    deviceid = models.CharField(max_length=255, blank=True, null=True);
    image = models.ImageField(upload_to=deviceimage)

    def __str__(self):
        return str(self.image)
